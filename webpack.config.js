var config = {
   entry: './src/main.js',
   output: {
      path:'/',
      filename: 'index.js',
   },
   devServer: {
      inline: true,
      port: 8080,
      hot: true,
      historyApiFallback: true,
   },
   mode: 'development',
   resolve: {
         extensions: ['*', '.js', '.jsx']
      },
   module: {
      rules: [
         {
            test: /\.jsx?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
            query: {
               presets: ['es2015', 'react']
            }
         }
      ]
   }
}
module.exports = config;