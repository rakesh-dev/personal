import { combineReducers } from 'redux';

import user from './userReducer';
import login from './loginReducer';
import userDetails from './getIndividualUserReducer';

let reducer = combineReducers({user,userDetails, login})

export default reducer;
