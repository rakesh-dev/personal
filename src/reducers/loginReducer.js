
function loginReducer(state={ account_details: { is_authenticated: false, errorMessage: ''} }, action) {
    switch(action.type) {
        case 'LOGIN_SUCCESS':
            return { account_details: { is_authenticated: true, errorMessage: ''} }
        case 'LOGIN_ERROR':
            return { account_details: { is_authenticated: false, errorMessage: action.data} }
        default:
            return state
        }
}

export default loginReducer;