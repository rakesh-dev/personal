
function userReducer(state=[], action) {
    switch(action.type) {
        case 'ADD_USER':
            return  [...state, action.userData]
        case 'LIST_USER':
            return action.list
        case 'EDIT_USER':
            return [ ...state.filter(user => user.id != action.data.id), 
                   Object.assign({}, action.data)]
        case 'DELETE_USER':
            return [...state.filter((user) => (user.id != action.id))]
        default:
            return state
        }
}

export default userReducer;