import { browserHistory } from 'react-router';

function getUserReducer(state=[], action) {
    switch(action.type) {
        case 'GET_USER':
            return Object.assign({}, action.id)
        default:
            return state
        }
}

export default getUserReducer;