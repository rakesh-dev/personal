import { service_config } from './config'
import { DOMAIN } from './config'
import { URL } from './config'

class UserService {

    static requestHeader() {
        return {
            'Content-Type': 'application/json',
        }
    }

    static getUsers() {
        let url = DOMAIN + URL.USER_LIST
        let request = new Request(url, {
                                    method: 'GET',
                                    })
        return fetch(request)//new Promise(resolve, reject).then((res)=>{console.log(res)}, (err)=>{console.log(err)});
                    .then((response) => {
                        return response.json()
                    })
                    .catch(error => {
                        return error
                    });
    }

    static addUsers(data) {
        let url = DOMAIN + URL.USER_CREATE
        let request = new Request(url, {
                                    method: 'POST',
                                    headers: this.requestHeader(),
                                    body: data
                                    })
        return fetch(request)
                    .then((response) => {
                        if(response.status != 201) {
                                throw response.statusText
                            }
                        return response.json()
                    })

    }

    static deleteUser(id) {
        let pk = Number(id)
        let url = DOMAIN + URL.USER_LIST + `${pk}/`
        let request = new Request(url, {
                                    method: 'DELETE',
                                    headers: this.requestHeader(),
                                    })
        return fetch(request)
                    .then((response) => {
                        return response.json()
                    })
                    .catch(error => {
                        return error
                    });
    }

    static getUser(pk) {
        let url = DOMAIN + USER_DETAIL
        let request = new Request(url, {
                                    method: 'GET',
                                    headers: this.requestHeader(),
                                    })
        return fetch(request)
                    .then((response) => {
                        return response.json()
                    })
                    .catch(error => {
                        return error
                    });
    }

    static editUser(data) {
        let pk = data.id
        delete data.id
        var data = JSON.stringify(data)
        let url = DOMAIN + URL.USER_LIST + `${pk}/`
        let request = new Request(url, {
                                    method: 'PUT',
                                    headers: this.requestHeader(),
                                    body: data
                                    })
        return fetch(request)
                    .then((response) => {
                        return response.json()
                    })
                    .catch(error => {
                        return error
                    });
    }

    static login(data) {
        let url = DOMAIN + URL.USER_LOGIN
        let post_data = Object.assign(data, service_config.client_details)
        data = new FormData();
        for (var key in post_data) {
            data.append(key,post_data[key])
        }
        let request = new Request(url, {
                                    method: 'POST',
                                    headers: {"Accept": 'application/json'},
                                    body: data
                                    })
        return fetch(request)
                            .then(function (response) {
                                if(response.status != 200) {
                                    throw response.statusText
                                }
                                return response.json()
                            })
    }

}


export default UserService;