import USER_ACTION_TYPE from './constAction'
import UserService from '../services/userServices'
import Authentication from '../Authentication/authentication'
import history from '../wrappers/wrappers'

export function updateListUserSuccess(list) {
    return {
        type: USER_ACTION_TYPE.LIST_USER,
        list
   }
}

export function listUser() {
    return function(dispatch) {
        return UserService.getUsers()
                            .then(response => {
                                dispatch(updateListUserSuccess(response.payload.results));
                            })
    }
}

export function addUserSuccess(userData) {
        return {
            type: USER_ACTION_TYPE.ADD_USER_SUCCESS,
            userData
    }
}

export function addUserError(error) {
        return {
            type: USER_ACTION_TYPE.ADD_USER_ERROR,
            error
    }
}



export function addUser(data) {
    return function(dispatch) {
        return UserService.addUsers(data)
                            .then(response => {
                                dispatch(addUserSuccess(response.payload))
                            })
                            .catch(error => {
                                dispatch(addUserError(error))
                            })
    }
}

export function deleteUserSuccess(id) {
        return {
            type: USER_ACTION_TYPE.DELETE_USER,
            id
    }
}

export function deleteUser(id) {
    return function(dispatch) {
        return UserService.deleteUser(id)
                            .then(response => {
                                dispatch(deleteUserSuccess(response.payload.id));
                            })
    }
}

export function getUserSuccess(id) {
        return {
            type: USER_ACTION_TYPE.GET_USER,
            id
    }
}

export function getUser(id) {
    return function(dispatch) {
        return UserService.getUser(id)
                            .then(response => {
                                dispatch(getUserSuccess(response.payload));
                            })
    }
}

export function editUserSuccess(data) {
        return {
            type: USER_ACTION_TYPE.EDIT_USER,
            data
    }
}

export function editUser(data) {
    return function(dispatch) {
        return UserService.editUser(data)
                            .then(response => {
                                dispatch(editUserSuccess(response.payload));
                            })
    }
}

export function loginError(data='Invalid Credential') {
        return {
            type: USER_ACTION_TYPE.LOGIN_ERROR,
            data
    }
}

export function loginSuccess(data='') {
        return {
            type: USER_ACTION_TYPE.LOGIN_SUCCESS,
            data
    }
}

export function login(data) {
    return function(dispatch) {
        return UserService.login(data)
                            .then(function (data) {
                                Authentication.authenticateUser(data.access_token);
                                history.push('/listEmployee')
                                dispatch(loginSuccess())
                            })
                            .catch(function (error) {
                                dispatch(loginError())
                            })
    }
}