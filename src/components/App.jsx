import React from 'react';
import { Provider } from 'react-redux'
import { Redirect } from 'react-router';
import { IndexRoute , Router, Switch, Route, Link } from 'react-router-dom';

import mainStore from '../store/mainStore';
import FileUpload from './fileUpload'
import AddUser from './users/addUser';
import ListUser from './users/listUser';
import EditUser from './users/editUser';
import Login from './account/Login';
import Authentication from '../Authentication/authentication'
import Header from './wrappers/Header'
import PrivateRoute from './account/privateRoute'
import history from '../wrappers/wrappers.js'

class App extends React.Component {
    render() {
        return (
                   <Provider store = {mainStore}>
                      <Router history={ history }>
                          <Switch>
                            <Route exact path='/' component={Login}/>
                            <Route component={AuthenticatedComponent}/>
                          </Switch>
                      </Router>
                    </Provider>
                  )
    }
}


const AuthenticatedComponent = () => (
                              <div>
                                  <Header/>
                                  <Switch>
                                    <PrivateRoute exact path='/addEmployee' component={AddUser}/>
                                    <Route exact path="/user/:id" component = {EditUser}/>
                                    <PrivateRoute exact path='/listEmployee' component={ListUser}/>
                                    <PrivateRoute exact path='/file-upload' component={FileUpload}/>
                                    <Route component={PageNotFound}/>
                                  </Switch>
                              </div>
  )

const PageNotFound = () => (<h1>Page not found</h1>)


export default App;