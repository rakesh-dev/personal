import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import * as userAction from '../../actions/userAction'
import Authentication from '../../Authentication/authentication'


class Login extends Component {

    constructor(props) {
        super(props)
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        e.preventDefault()
        let email = this.refs.email.value
        let password = this.refs.password.value
        let data = {
            "username": email,
            "password": password
        }
        this.props.action.login(data)
    }

    render(){
        return (
                <div className="col-md-4 col-md-offset-4" Style='border-style: groove; border-color: text-primary; border-width: 5px; margin-top:10%'>
                    <center><h2>Login</h2></center>
                    <p className='danger'>{this.props.account_details.errorMessage}</p>
                    <form>
                        <div className="form-group">
                            <label>
                                Username:
                            </label>
                            <input className="form-control" type="text" name="email" ref='email' placeholder='Username'/>
                        </div>
                        <div className="form-group">
                            <label>
                                Password:
                            </label>
                            <input className="form-control" type="password" name="password" placeholder='*******' ref='password'/>
                    </div>
                        <center>
                            <button className='btn btn-primary' onClick = { (e) => this.handleClick(e)}>
                                Submit
                            </button>
                        </center>
                    </form>
                </div>
                )
    }

}

function mapDispatchToProps(dispatch) {
    return { 
        "action": bindActionCreators(userAction, dispatch)
    }
}

function mapStateToProps(state) {
    return {
        "account_details": state.login.account_details
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Login);