import React from 'react';

import { Route } from 'react-router-dom';

import Authentication from '../../Authentication/authentication'


const PrivateRoute = (...rest) => {
  let Component = rest[0].component
  let props = rest[0]
  return (<Route  
                  render= { props => Authentication.isAuthenticatedUser() == true ? 
                    (<Component {...props}/>) : 
                    (<Redirect to={{ pathname: "/", }} />)
    }
  />
)};

export default PrivateRoute;