import React, { Component } from 'react'
import { connect } from 'react-redux'

const Options = (props) => (<option value={props.value}>{props.value}</option>)

export default Options;