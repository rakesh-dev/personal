import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import {browserHistory, Redirect } from 'react-router';


import Authentication from '../../Authentication/authentication'


class Header extends Component {
    
    logout() {
        Authentication.deauthenticateUser(); 
    }

    render(){
        return (
                <nav className="navbar navbar-inverse">
                    <ul className="nav navbar-nav">
                        <li><Link to={'/addEmployee'}>Create Employee</Link></li>
                        <li><Link to={'/listEmployee'}>List Employee</Link></li>
                        <li><Link to={'/file-upload'}>File Upload</Link></li>
                        <li><Link onClick={() => this.logout()} to={'/'}>Log out</Link></li>
                    </ul>
                </nav>
                )
    }

}

export default Header;