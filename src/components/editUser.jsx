import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import Options from './Options.jsx'
import * as userAction from '../actions/userAction.js'
import UserForm from './userForm.jsx';
import Header from './Header.jsx'



class EditUser extends React.Component {
    constructor(props) {
        super(props);
        this.validateField = this.validateField.bind(this);
        this.handleClick = this.handleClick.bind(this);
        let id = this.props.match.params.id
        let user_details = this.props.user_details.filter((user) => user.id == id)
        this.state = {
            'id': user_details[0].id,
            'userName': user_details[0].username,
            'email': user_details[0].email,
            'designation': user_details[0].first_name,
            'department': user_details[0].last_name,
            'isStaff': '',
            'dateOfJoining': '',
            'formError': { 'emailError':'', 
                           'dateOfJoiningError': '', 
                           'isDateOfJoiningValid':true,
                           'isEmailValid':true, 
                           'userNameError':'',
                           'designationError':'',
                           'departmentError':'',
                           'staffError':'',
                         },
            'formValid': true,
        }
    }

    render(){
        const designation_array = [
                                    '--Select Designation--',
                                    'Software Engineer', 
                                    'Senior Software Engineer', 
                                    'Manager', 
                                    'Team Lead', 
                                    'Module Lead', 
                                    'Architect'  ]
        const dept_array = [
                            '--Select Dept--',
                            'LAMP',
                            'Java',
                            'Testing',
                            'DotNet']
        let formDivStyle = { width: "60%" }
        console.log(this.state)
        return (
            <div>
                <Header/>
                <UserForm designation_list={designation_array}
                          dept_array={dept_array}
                          handleClick={this.handleClick}
                          form_data={this.state.users}
                          validateField={this.validateField}
                          title={'Edit - '+ this.state.userName} />
            </div>)
    }

    validateField(e){
        switch(e.target.name) {
            case 'userName':
                this.setState({ 'userName': e.target.value.toUpperCase()})
                this.state.formError.userNameError = e.target.value == ''? '': 'Success';
                break
            case 'email':
                this.setState({ 'email': e.target.value })
                let emailValid = e.target.value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
                let isEmailValid = emailValid ? true : false;
                if(e.target.value == '') {
                    var emailError = ''
                }
                else {
                    var emailError = emailValid ? 'Success' : 'Invalid Email'
                }
                this.state.formError.emailError = emailError
                this.state.formError.isEmailValid = isEmailValid
                break
            case 'designation':
                this.setState({ 'designation': e.target.value })
                this.state.formError.designationError = e.target.value == '--Select Designation--'? '': 'Success';
                break
            case 'department':
                this.setState({ 'department': e.target.value })
                this.state.formError.departmentError = e.target.value == '--Select Dept--'? '': 'Success'
                break
        }
        this.state.formValid = this.state.formError.isEmailValid && this.state.formError.isDateOfJoiningValid ? true: false;

    }

    handleClick(e) {
        e.preventDefault()
        let userNameNode = this.state.userName
        let emailNode = this.state.email
        let designationNode = this.state.designation
        let departmentNode = this.state.department
        let isConfirmedNode = this.state.isConfirmed
        let dateOfJoining = this.state.dateOfJoining
        let data = {
                "id": Number(this.props.match.params.id),
                "username": userNameNode.trim(),
                "email": emailNode.trim(),
                "password": "aspire@123",
                "first_name": designationNode.trim(),
                "last_name": departmentNode.trim(),
        }
        this.props.action.editUser(data)
        this.props.history.push('/listEmployee')
    }
}

function mapDispatchToProps(dispatch) {
    return {
        action: bindActionCreators(userAction, dispatch)
    }
}

function mapStateToProps(state) {
    return {
        "user_details": state.user
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(EditUser)

