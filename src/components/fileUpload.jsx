import React from 'react';

import Header from './wrappers/Header'



class FileUpload extends React.Component {
   render() {
      return (
            <div className="col-md-4 col-md-offset-4" Style='border-style: groove; border-color: text-primary; border-width: 5px;'>
                <form onSubmit={this.onFormSubmit}>
                    <h3>File Upload</h3>
                    <input className="form-control" type="file" onChange={this.onChange}/>
                    <button className='btn btn-primary' type="submit">
                        <span className='glyphicon glyphicon-cloud-upload'></span>Upload
                    </button>
                </form>
            </div>
      );
   }
}
export default FileUpload;