import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as userAction from '../../actions/userAction'
import UserForm from './userForm';


class AddUser extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            'userName': '',
            'email': '',
            'designation': '',
            'department': '',
            'formError': { 'emailError':'', 
                           'dateOfJoiningError': '', 
                           'isEmailValid':'', 
                           'userNameError':'',
                           'designationError':'',
                           'departmentError':'',
                           'staffError':'',
                         },
            'formValid': false,
        }
        this.validateField = this.validateField.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    render(){
        const designation_array = [
                                    '--Select Designation--',
                                    'Software Engineer', 
                                    'Senior Software Engineer', 
                                    'Manager', 
                                    'Team Lead', 
                                    'Module Lead', 
                                    'Architect'  ]
        const dept_array = [
                            '--Select Dept--', 
                            'LAMP', 
                            'Java', 
                            'Testing', 
                            'DotNet']
        let formDivStyle = { width: "60%" }
        return (
            <div>
                <UserForm designation_list={designation_array} 
                          dept_array={dept_array} 
                          handleClick={this.handleClick}
                          form_data={this.state}  
                          validateField={this.validateField} />
            </div>)
    }

    validateField(e){
        switch(e.target.name) {

            case 'userName':
                this.setState({ 'userName': e.target.value.toUpperCase()})
                this.state.formError.userNameError = e.target.value == ''? '': 'Success';
                break
            case 'email':
                this.setState({ 'email': e.target.value })
                let emailValid = e.target.value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)
                let isEmailValid = emailValid ? true : false;
                if(e.target.value == '') {
                    var emailError = ''
                }
                else {
                    var emailError = emailValid ? 'Success' : 'Invalid Email'
                }
                this.state.formError.emailError = emailError
                this.state.formError.isEmailValid = isEmailValid
                break
            case 'designation':
                this.setState({ 'designation': e.target.value })
                this.state.formError.designationError = e.target.value == '--Select Designation--'? '': 'Success';
                break
            case 'department':
                this.setState({ 'department': e.target.value })
                this.state.formError.departmentError = e.target.value == '--Select Dept--'? '': 'Success'
                break
        }

        this.state.formValid = this.state.formError.isEmailValid ? true: false;


    }

    handleClick(e) {
        e.preventDefault()
        let userNameNode = this.state.userName
        let emailNode = this.state.email
        let designationNode = this.state.designation
        let departmentNode = this.state.department
        let isConfirmedNode = this.state.isConfirmed
        let dateOfJoining = this.state.dateOfJoining
        let data = {
                "username": userNameNode.trim(),
                "email": emailNode.trim(),
                "password": "aspire@123",
                "first_name": designationNode.trim(),
                "last_name": departmentNode.trim(),          
        }
        this.props.action.addUser(JSON.stringify(data))
        this.props.history.push('/listEmployee')
    }
}

function mapDispatchToProps(dispatch) {
    return {
        action: bindActionCreators(userAction, dispatch)
    }
}

export default connect(null, mapDispatchToProps)(AddUser)

