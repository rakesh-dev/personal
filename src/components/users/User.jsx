import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as userAction from '../../actions/userAction'
import { Link } from 'react-router-dom';


class User extends React.Component {

    render(){
        return (
          <tr>
                <td>{ this.props.username }</td>
                <td>{ this.props.email }</td>
                <td>{ this.props.designation }</td>
                <td>{ this.props.department }</td>
                <td>
                    <Link to = {'/user/'+this.props.id+'/'} className='btn btn-primary' > 
                        <span className="glyphicon glyphicon-edit"></span>&nbsp;&nbsp;Edit
                    </Link>&nbsp;&nbsp;
                    <button ref='click_element' 
                            id={this.props.id} 
                            className='btn btn-primary' 
                            onClick= { (e) => this.onDelete(e)}>
                            <span className="glyphicon glyphicon-remove-circle"></span>&nbsp;&nbsp;Delete
                    </button>
                </td>
          </tr>
        )
    }

    onDelete(e){
        let id = this.refs.click_element.id
        this.props.action.deleteUser(id)
    }
    
}

function mapDispatchToProps(dispatch) {
    return {
        action: bindActionCreators(userAction, dispatch)
    }
}

export default connect(null, mapDispatchToProps)(User)