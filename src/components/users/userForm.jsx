import React from 'react';

import Options from '../wrappers/Options'


class UserForm extends React.Component {

    render(){
        let formDivStyle = { width: "100%" }
        return (
            <div className="col-md-4 col-md-offset-4" Style='border-style: groove; border-color: text-primary; border-width: 5px;'>
                <form style={ formDivStyle }>
                    <center><h2>{ this.props.title }</h2></center>
                    <div className="form-group">
                        <label>
                            Employee Name*:
                        </label>
                        <input className="form-control" onChange={this.props.validateField} type="text" name="userName" ref= 'username' value={this.props.form_data.userName} ref='userName'/>
                        <p> { this.props.form_data.formError.userNameError }</p>
                    </div>
                    <div className="form-group">
                        <label>
                            Employee Email*:
                        </label>
                        <input className="form-control" onChange={this.props.validateField} type="email" name="email" ref='email' value={this.props.form_data.email}/>
                        <p> { this.props.form_data.formError.emailError }</p>
                    </div>
                    <div className="form-group" name="designation">
                        <lable>
                        Designation*:
                        </lable>
                          <select className="form-control" value={ this.props.form_data.designation } onChange={this.props.validateField} name='designation' ref='designation'>
                          { this.props.designation_list.map(value => <Options value={value} />) }
                          </select>
                        <p> { this.props.form_data.formError.designationError }</p>
                    </div>
                    <div className="form-group">
                        <lable>
                         Department:
                        </lable>
                        <select className="form-control" value={ this.props.form_data.department } onChange={this.props.validateField} name="department" ref='department'>
                            { this.props.dept_array.map(value => <Options value={value} />) }
                        </select>
                        <p> { this.props.form_data.formError.departmentError }</p>
                    </div>
                        <center>
                            <button className='btn btn-primary' disabled={!this.props.form_data.formValid} onClick = { (e) => this.props.handleClick(e)}>
                                Submit
                            </button>
                        </center>
                </form>
            </div>)
    }

}


export default UserForm;