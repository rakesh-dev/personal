import React from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import * as userAction from '../../actions/userAction';
import User from './User';

class ListUser extends React.Component {

   componentDidMount() {
        this.props.action.listUser()
   }

    render(){
        return (
            <center>
                <div Style='border-style: groove; border-color: text-primary; border-width: 5px; width: 80%; height: 100%'>
                    <center><h3>Employee List</h3></center>
                    <center><table className='table table-bordered' Style='width:98%' >
                    <thead className='thead-dark'>
                        <tr>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Designation</th>
                            <th>SL</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody >
                        { this.props.user.map((data) => <User designation={data.first_name} 
                            department={data.last_name} 
                            email={data.email} 
                            username={data.username} 
                            id={data.id}/>) }
                    </tbody>
                </table>
                </center>
            </div>
            </center>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        action: bindActionCreators(userAction, dispatch)
    }
}

function mapStateToProps(state) {
    return state
}

export default connect(mapStateToProps, mapDispatchToProps)(ListUser)